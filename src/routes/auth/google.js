const express = require('express')
const passport = require('passport')
const router = express.Router()

router.get('/', passport.authenticate('google', { scope: ['profile', 'email'] }))

router.get('/callback',
    passport.authenticate('google', { failureRedirect: '/', successRedirect: '/payment.html' }),
)

router.post('/logout', function(req, res, next) {
    req.logout();
    res.redirect('/');
  });

module.exports = router;