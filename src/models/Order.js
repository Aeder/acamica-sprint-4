const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    sequelize.define('Order', {
        price: {
            type: DataTypes.DOUBLE,
            defaultValue: 0
        },
        state: {
            type: DataTypes.ENUM,
            values: ['PENDING', 'CONFIRMED', 'PROCESSING', 'SENT', 'DELIVERED'],
            defaultValue: 'PENDING'
        },
        address: DataTypes.STRING
    });
}