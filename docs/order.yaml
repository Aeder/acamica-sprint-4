paths:
  /api/orders:
    get:
      tags:
        - Order
      security:
        - BearerAuth: []
      summary: Permite ver la lista completa de pedidos
      description: Permite a un usuario administrador ver la lista completa de pedidos realizados.
      responses:
        200:
          description: Lista de todos los pedidos
          content:
            application/json:
              schema:
                $ref: '#components/schemas/OrderList'
    post:
      tags:
        - Order
      security:
        - BearerAuth: []
      summary: Permite al usuario realizar un pedido
      description: Permite al usuario ingresar los parametros necesarios para realizar un pedido.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/OrderDetails'
      responses:
        200:
          description: Pedido creado
  /api/orders/history:
    get:
      tags:
        - Order
      security:
        - BearerAuth: []
      summary: Permite a un usuario ver su historial de pedidos.
      description: Permite a un usuario ver todos los pedidos que ha realizado.
      responses:
        200:
          description: Lista de todos los pedidos
          content:
            application/json:
              schema:
                $ref: '#components/schemas/OrderList'
  /api/orders/{id}:
    patch:
      tags:
        - Order
      security:
        - BearerAuth: []
      summary: Permite a un administrador modificar un pedido
      description: Permite a un administrador modificar los parametros de un pedido.
      parameters:
        - name: id
          in: path
          description: ID del pedido.
          type: integer
        - name: Nuevo estado del pedido
          in: body
          properties:
            data:
              type: object
              properties:
                state:
                  type: string
                  enum: ['PENDING', 'CONFIRMED', 'PROCESSING', 'SENT', 'DELIVERED']

      responses:
        200:
          description: Pedido modificado
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SimpleOrderDetails'
components:
  schemas:
    OrderDetails:
      type: object
      required:
        - payment_method
        - products
      properties:
        payment_method:
          type: integer
          description: ID del método de pago
        address:
          type: string
          example: Calle Falsa 548
        products:
          type: array
          items:
            type: object
            properties:
              id:
                type: integer
                description: ID del producto
                example: 0
              amount:
                type: integer
                description: Cantidad del producto
                example: 10            
    SimpleOrderDetails:
      type: object
      required:
        - id
      properties:
        id:
          type: integer
        price: 
          type: number
        state:
          type: string
          enum: ['PENDING', 'CONFIRMED', 'PROCESSING', 'SENT', 'DELIVERED']
        address:
          type: string
        createdAt:
          type: string
          description: Fecha y hora de creación del pedido 
          example: 2021-10-27T23:58:00.000Z
        updatedAt:
          type: string
          description: Fecha y hora de modificación del pedido 
          example: 2021-10-27T23:58:00.000Z
        user_id:
          type: integer
        payment_method_id:
          type: integer

    OrderList:
      type: array
      items:
        type: object
        properties:
          id:
            type: integer
            description: ID del pedido
            example: 0
          price:
            type: number
            description: Precio total del pedido
            example: 200.50
          state:
            type: string
            description: Estado del pedido
            example: CONFIRMED
          adress:
            type: string
            description: Direccion opcional del pedido
            example: Calle Buena 123
          createdAt:
            type: string
            description: Fecha y hora de creación del pedido 
            example: 2021-10-27T23:58:00.000Z
          updatedAt:
            type: string
            description: Fecha y hora de modificación del pedido 
            example: 2021-10-27T23:58:00.000Z
          user_id:
            type: integer
            description: ID del usuario que hizo el pedido
            example: 0
          payment_method_id:
            type: integer
            description: ID del metodo de pago
            example: 2
          Products:
            type: array
            items:
              type: object
              properties:
                id:
                  type: integer
                  description: ID del producto
                  example: 0
                name:
                  type: string
                  description: Nombre del producto
                  example: Milanesa
                category:
                  type: string
                  description: Categoria del producto
                  example: Alimento Fresco
                price:
                  type: number
                  description: Precio del producto
                  example: 99.99
                createdAt:
                  type: string
                  description: Fecha y hora de creación del producto 
                  example: 2021-10-27T23:58:00.000Z
                updatedAt:
                  type: string
                  description: Fecha y hora de modificación del producto 
                  example: 2021-10-27T23:58:00.000Z
                OrderProducts:
                  type: object
                  properties:
                    amount:
                      type: integer
                      description: Cantidad del producto en la orden
                      example: 99