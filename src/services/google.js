const GoogleStrategy = require("passport-google-oauth20").Strategy;
const { User } = require("../db/connection").models;

const ACTUAL_URL = process.env.SITE_URL ? process.env.SITE_URL : "http://127.0.0.1:5000" 

module.exports = new GoogleStrategy(
  {
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: ACTUAL_URL + "/auth/google/callback",
  },
  async (accessToken, refreshToken, profile, cb) => {
    try {
      const [user, created] = await User.findOrCreate({
        where: { email: profile.emails[0].value },
        defaults: {
          username: profile.name.givenName,
          password: "googleuser",
          admin: false,
          email: profile.emails[0].value,
        },
      });
      console.log(created);
      if (created) {
        return cb(null, user.get({ plain: true }));
      } else {
        if (user) {
          return cb(null, user.get({ plain: true }));
        } else {
          console.log(user.get({ plain: true }));
          return cb("Error when finding or creating user", null);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
);
