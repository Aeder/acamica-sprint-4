const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  dialect: 'mariadb'
});
const createModelsRelationships = require('../models/modelsRelationships')

const testConnection = async () =>
{
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

async function start() {
  testConnection();
  await sequelize.sync({force: false});
  console.log("All models were synchronized successfully.");
}

const model_definition_list = [
  require('../models/Order'),
  require('../models/OrderProducts'),
  require('../models/PaymentMethod'),
  require('../models/Product'),
  require('../models/User')
]

console.log("Defining models...");
for (const model_definition of model_definition_list ){
  model_definition(sequelize);
}
console.log("Finished defining models.");

console.log("Defining relationships...");
createModelsRelationships(sequelize);
console.log("Finished defining relationships.")

start();

module.exports = sequelize;