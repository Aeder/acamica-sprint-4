FROM node:17

ENV NODE_ENV=production

WORKDIR /var/app
COPY . /var/app/

RUN npm install
CMD ["node", "index.js"]