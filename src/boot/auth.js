const passport = require("passport");
const google_strategy = require("../services/google");
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET

// Load strategies into passport
module.exports = function () {
  passport.serializeUser(function (user, cb) {
    process.nextTick(function () {
      user_info = { user_id: user.id };
      token = jwt.sign(user_info, jwtSecret);
      cb(null, { token: token });
      // cb(null, { id: user.id });
    });
  });

  passport.deserializeUser(function (user, cb) {
    process.nextTick(function () {
      return cb(null, user);
    });
  });
  passport.use(google_strategy);
};
