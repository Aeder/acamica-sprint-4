const router = require('express').Router()

router.use('/mercadopago', require('./mercadopago'))

module.exports = router;