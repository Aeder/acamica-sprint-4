# acamica-sprint-4

Proyecto para el cuarto sprint de Acamica

# Instalación en Linux (Fedora)

1. Asegurarse de tener git, redis y npm instalados:

```
sudo dnf install npm git redis
```

2. Ejecutar git clone en la carpeta donde queremos descargar el programa

```
cd mi-carpeta
git clone https://gitlab.com/Aeder/acamica-sprint-4.git
```

3. Instalar las librerias necesarias
```
cd acamica-sprint-4
npm install
```

# Configuración

1. Crear un archivo .env copiando el contenido de ejemplo de .env.example

```
cp .env.example .env
```

2. Completar las variables usando nano, vim u otro editor de texto:

* DB_HOST : Dirección IP o URL del servidor de base de datos MariaDB
* DB_USER : Usuario de la base de datos
* DB_PASS : Contraseña del usuario de base de datos
* DB_NAME : Nombre de la base de datos a usar 
* JWT_SECRET : Secreto para firmar los tokens JWT. Preferiblemente debe ser largo y difícil de predecir.
* REDIS_HOST : Direccion IP o URL del servidor de cache Redis.
* REDIS_PORT : Puerto empleado para conectarse al servidor Redis.

* GOOGLE_CLIENT_ID: ID obtenido a traves de las credenciales de Google
* GOOGLE_CLIENT_SECRET: Secreto obtenido a traves de las credenciales de Google

* MP_ACCESS_TOKEN= Token de acceso obtenido a traves de las credenciales de MercadoPago

# Correr el programa

1. Asegurarse que MariaDB y Redis esten funcionando. Para ello ejecutar los siguientes comandos (En sistemas linux con systemd)

```
sudo systemctl start mariadb
sudo systemctl start redis
```

2. Luego, para correr el programa ejecutar el siguiente comando en consola

```
npm run start
```

# Correr el programa en Docker

1. Asegurarse de estar en la misma carpeta donde se encuentra el dockerfile

```
cd acamica-sprint-4
```

2. Instalar docker siguiendo las instrucciones en la [página oficial](https://docs.docker.com/engine/install/)

3. Asegurarse de que docker este funcionando

```
sudo systemctl start docker
```

4. Ejecutar el proceso de creacion del contenedor, etiquetandolo como ```acamica-sprint:docker```

```
sudo docker build . -t acamica-sprint:docker
```

5. Tomar nota del nombre resultante del contenedor y ejectuar la aplicacion

```
sudo docker run acamica-sprint:docker
```

# Correr el programa con docker-compose

Si es necesario desplegar toda la infrastructura para pruebas, es mejor emplear el archivo docker-compose.yml

1. Realizar los pasos 1 a 3 de la sección anterior

2. Instalar docker-compose usando la [guia oficial](https://docs.docker.com/compose/install/#install-compose)

3. Levantar toda la infraestructura junto a a la API

```
sudo docker-compose up -d
```

Al finalizar las pruebas, es posible detener todo con el siguiente comando

```
sudo docker-compose stop
```

Para dar de baja todo incluyendo volumenes, ejecutar el siguiente comando:

```
sudo docker-compose down --volumes
```

# Corrrer docker sin usar sudo

1. Agregar el grupo docker si no existe:

 ```
 sudo groupadd docker
 ```

2. Agregar al usuario connectado "$USER" al grupo docker. Cambia el nombre de usuario ($USER) si quieres usar un usuario que no sea el actual:

 ```
 sudo gpasswd -a $USER docker
 ```

3. Correr el comando ```newgrp docker``` o cerrar y abrir la sesión de usuario para aplicar los cambios.

Nota: Se considera que correr docker de esta manera es inseguro porque otorga al usuario acceso root sin contraseña al socket de docker. 

# Pruebas

1. Si se desea ejecutar las prueba se debe usar el siguiente commando

```
npm run test
```