const { User } = require("../db/connection").models;

class UserController {
  createUser = async function (new_user_data) {
    try {
      await User.create({
        username: new_user_data.username,
        password: new_user_data.password,
        admin: false,
        full_name: new_user_data.full_name,
        phone: new_user_data.phone,
        email: new_user_data.email,
        address: new_user_data.address,
      }).then((result) => {
        if (result) {
          return result;
        } else {
          return false;
        }
      });
    } catch (e) {
      console.log("Error while creating new user: " + e);
      return false;
    }
  };

  findUser = async function (user_email) {
    console.log("Searching email: " + user_email);
    try {
      await User.findOne({
        where: { email: user_email },
        raw: true,
      }).then((result) => {
        if (result != null) {
          console.log("UserController -> findUser - Result: ", result);
          return result;
        } else {
          console.log("User not found");
          return false;
        }
      });
    } catch (e) {
      console.log("Error while finding user:" + e);
      return false;
    }
  };
}

module.exports = UserController;
