const express = require("express");
const router = express.Router();
const mercadopago = require("mercadopago");

router.post("/process_payment", (req, res) => {
  console.log("Payment attempted");
  mercadopago.configurations.setAccessToken(process.env.MP_ACCESS_TOKEN);
  const data = {
    ...req.body,
  };
  mercadopago.payment
    .save(data)
    .then(function (response) {
      const { status, status_detail, id } = response.body;
      console.log("Payment successful");
      res.status(response.status).json({ status, status_detail, id });
    })
    .catch(function (error) {
      console.error(error);
      res.status(406).json({ message: "Error during payment" });
    });
});

module.exports = router;
