const jwt = require('jsonwebtoken');
const { models } = require('../db/connection');
const jwtSecret = process.env.JWT_SECRET


async function isAdmin(req, res, next) {

    try {
        const jwt_token = req.headers.authorization.split(' ')[1];
        decoded_token = jwt.verify(jwt_token, jwtSecret);
        if (decoded_token) {
            await models.User.findOne({
                where: {
                    id: decoded_token.user_id,
                    admin: true
                }
            }).then((result) => {
                if (result == null) {
                    res.status(403).json("Usuario administrador no encontrado").end();
                } else {
                    next();
                }
            });
        } else {
            res.status(403).json("Debe proveer sus credenciales de admin para acceder a este recurso").end();
        }
    } catch (e) {
        console.log(e);
        res.status(500).json("Error al identificar su usuario").end();
    }

};

async function isLogged(req, res, next) {
    try {
        const jwt_token = req.headers.authorization.split(' ')[1];
        decoded_token = jwt.verify(jwt_token, jwtSecret);

        if (decoded_token) {
            await models.User.findOne({
                where: {
                    id: decoded_token.user_id
                }
            }).then((result) => {
                if (result == null) {
                    res.status(403).json("El usuario no existe").end();
                } else {
                    req.user_id = result.id;
                    next();
                }
            });
        } else {
            res.status(403).json("Debe proveer sus credenciales para acceder a este recurso").end();
        }

    } catch (e) {
        console.log(e);
        res.status(500).json("Error al identificar su usuario").end();
    }
};

module.exports = {
    isAdmin: isAdmin,
    isLogged: isLogged
}