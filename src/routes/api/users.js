const express = require('express');
var router = express.Router();
const { models } = require('../../db/connection');
const UserController = require('../../controllers/UserController')
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET

router.post('/', async (req, res) => {
    const new_user_data = req.body;

    result = UserController.createUser(new_user_data);

    if (result) {
        res.status(200).json(result);
    } else {
        res.status(500).json("El email seleccionado ya esta en uso")
    }
})

router.post('/login', async (req, res) => {

    const { request_email, request_password } = req.body;
    console.log(req.body);

    let requested_user;

    try {
        requested_user = await models.User.findOne({
            where: {
                email: request_email
            }
        });
        console.log(requested_user)

    } catch (e) {
        console.log("Couldn't find the requested user:" + e)
    }

    if (requested_user == null) {
        res.status(404).send("No existe el usuario");
    } else {
        if (request_password != requested_user.password) {
            res.status(403).send("Contraseña incorrecta");
        } else {
            user_info = { user_id: requested_user.id };
            token = jwt.sign(user_info, jwtSecret);
            res.status(200).json({ token: token });
        }
    }

});

module.exports = router;