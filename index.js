const dotenv = require('dotenv').config();
const express = require("express");
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const passport = require('passport')
const session = require('express-session');
require('./src/boot/auth')()


app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(session({
    secret: process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
  }))


const swaggerOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        components: {
            securitySchemes: {
                BearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT'
                }
            }
        },
        info: {
            title: 'Acamica Sprint 4 API',
            version: '2.1.2'
        }
    },
    apis: [
        './docs/product.yaml',
        './docs/user.yaml',
        './docs/order.yaml',
        './docs/payment_method.yaml',
        './docs/payment.yaml',
        './docs/auth.yaml'
    ],
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

app.use('/', express.static(__dirname + '/public'));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(passport.initialize())
app.use(passport.session())

app.use('/api', require('./src/routes/api'));
app.use('/auth', require('./src/routes/auth'));
app.use('/payment',require('./src/routes/payment'))

app.listen(5000, () => { console.log("Corriendo en http://127.0.0.1:5000/") });

module.exports = app;