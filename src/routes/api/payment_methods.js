const express = require('express');
const router = express.Router();
const { models } = require('../../db/connection');
const middleware = require('../../middleware/index');

router.get('/', async (req, res) => {
    try {
        await models.PaymentMethod.findAll().then((result) => {
            res.status(200).json(result);
        })
    } catch (e) {
        console.log(e);
        res.status(500);
    }
})

router.post('/', middleware.isAdmin, async (req, res) => {
    const new_payment_method = req.body;

    try {
        await models.PaymentMethod.create({
            name: new_payment_method.name
        }).then((result) => {
            res.status(200).json(result);
        })
    } catch (e) {
        console.log(e);
        res.status(500);
    }
})

router.patch('/:id', middleware.isAdmin, async (req, res) => {
    const new_payment_method_values = req.body
    try {
        await models.PaymentMethod.findOne({
            where: {
                id: req.params.id
            }
        }).then(async (result) => {
            Object.keys(new_payment_method_values).forEach(key => {
                result[key] = new_payment_method_values[key];
            });
            await result.save().then((result) => {
                res.status(200).json(result)
            })
        })
    } catch (e) {
        console.log(e);
        res.status(500)
    }
})

router.delete('/:id', middleware.isAdmin, async (req, res) => {
    try {
        await models.PaymentMethod.destroy({
            where: {
                id: req.params.id
            }
        }).then((result) => {
            res.status(201).json("Metodo de pago borrado");
        })
    } catch (e) {
        console.log(e);
        res.status(500);
    }
})

module.exports = router;